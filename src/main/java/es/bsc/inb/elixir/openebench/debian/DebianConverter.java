/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.debian;

import es.bsc.inb.elixir.biotools.model.LicenseType;
import es.bsc.inb.elixir.openebench.model.tools.Publication;
import es.bsc.inb.elixir.openebench.model.tools.Tool;
import es.bsc.inb.elixir.openebench.model.tools.ToolType;
import es.bsc.inb.elixir.openebench.model.tools.Web;
import es.bsc.inb.elixir.openebench.repository.OpenEBenchEndpoint;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

public class DebianConverter {
   
    public static Tool convert(Map<String, Object> attributes) {
        
        String id = null;

        final Object _registries = attributes.get("registries");
        if (_registries instanceof List) {
            List registries = (List)_registries;
            for (Object _registry : registries) {
                if (_registry instanceof Map) {
                    final Map egistry = (Map)_registry;
                    final Object _id = egistry.get("entry");
                    final Object registry = egistry.get("name");
                    if (_id instanceof String && "bio.tools".equals(registry)) {
                        id = _id.toString().toLowerCase().trim();
                        break;
                    }
                }
            }
        }

        StringBuilder idTemplate = new StringBuilder(OpenEBenchEndpoint.TOOL_URI_BASE).append("debian:").append(id);
        
        final Object version = attributes.get("version");
        if (version instanceof String) {
            idTemplate.append(':').append(version.toString().replace(' ', '_'));
        }
        idTemplate.append('/').append(ToolType.COMMAND_LINE.type);

        URI homepage = null;
        final Object _homepage = attributes.get("homepage");
        if (_homepage instanceof String) {
            try {
                 homepage = URI.create(_homepage.toString());
                 idTemplate.append('/').append(homepage.getHost());
             } catch (IllegalArgumentException ex) {
                 Logger.getLogger(DebianConverter.class.getName()).log(Level.INFO, "invalid homepage: {0}", _homepage);
             }
        }
        
        final Tool tool = new Tool(URI.create(idTemplate.toString()), ToolType.COMMAND_LINE.type);
        
        tool.setName(id);
        
        if (homepage != null) {
            Web web = new Web();
            web.setHomepage(homepage);
            tool.setWeb(web);
        }

        Object _package = attributes.get("package");
        if (_package instanceof String) {
            tool.setExternalId("debian:" + _package.toString());
        }

        setLicense(tool, attributes.get("license"));
        setDescription(tool, attributes.get("descr"));
        addPublications(tool, attributes.get("bib"));

        return tool;
    }
    
    private static void setLicense(Tool tool, Object license) {
        if (license instanceof String) {
            try {
                LicenseType.fromValue(license.toString()); // just to check if valid.
            } catch(IllegalArgumentException ex) {
                Logger.getLogger(DebianConverter.class.getName()).log(Level.INFO, "unrecognized license: {0}", license);
            }
            tool.setLicense(license.toString());
        }
    }
    
    private static void setDescription(Tool tool, Object description) {
        if (description instanceof List) {
            for (Object obj : (List)description) {
                if (obj instanceof Map) {
                    final Map _descr = (Map)obj;
            
                    final Object _description = _descr.get("description");
                    if (_description instanceof String) {
                        tool.setShortDescription(_description.toString());
                    }

                    final Object _long_description = _descr.get("long_description");
                    if (_long_description instanceof String) {
                        tool.setDescription(_long_description.toString());
                    }

                    if (tool.getExternalId() == null) {
                        final Object _package = _descr.get("package");
                        if (_package instanceof String) {
                            tool.setExternalId("debian:" + _package.toString());
                        }
                    }
                    
                    break; // !!! suppose that first descr is in english
                }
            }
        }
    }
    
    private static void addPublications(Tool tool, Object bibliography) {
        if (bibliography instanceof List) {
            List<Publication> publications = tool.getPublications();
            if (publications == null) {
                tool.setPublications(publications = new ArrayList());
            }

            for (Object obj : (List)bibliography) {
                if (obj instanceof Map) {
                    final Publication publication = new Publication();
                    publications.add(publication);
                    
                    final Map entry = (Map)obj;
                    final Object _key = entry.get("key");
                    final Object _value = entry.get("value");    
                    if ("doi".equals(_key)) {
                        publication.setDOI(_value.toString());
                    }
                }
            }
        }
    }
}
