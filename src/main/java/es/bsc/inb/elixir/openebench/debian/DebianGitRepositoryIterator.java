/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.debian;

import com.google.common.jimfs.Jimfs;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.util.SystemReader;
import org.yaml.snakeyaml.Yaml;

/**
 * @author Dmitry Repchevsky
 */

public class DebianGitRepositoryIterator 
        implements Iterator<Map<String, Object>>, Closeable {

    private final DirectoryStream<Path> directories;
    private final Iterator<Path> iterator;
    private final List<Path> file_paths;
    
    public DebianGitRepositoryIterator(
            final String git, 
            final String branch, 
            final String path) throws IOException {

        final int idx = branch.lastIndexOf('/');
        final String local = idx < 0 ? branch : branch.substring(idx + 1);
        
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        com.google.common.jimfs.Configuration config = 
                com.google.common.jimfs.Configuration.unix().toBuilder()
                        .setWorkingDirectory("/")
                        .build();

        final FileSystem fs = Jimfs.newFileSystem(config);
        final Path root = fs.getPath("/");
        
        try (Git g = Git.cloneRepository()
                          .setURI(git)
                          .setDirectory(root)
                          .setBranch(local)
                          .setRemote(branch)
                          .call()) {
        } catch (GitAPIException ex) {
            Logger.getLogger(DebianGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
            throw new IOException(ex);
        }

        file_paths = new ArrayList();
        directories = Files.newDirectoryStream(root.resolve(path));
        iterator = directories.iterator();
    }
    
    @Override
    public boolean hasNext() {
        if (file_paths.size() > 0) {
            return true;
        }
        
        while (iterator.hasNext()) {
            final Path directory = iterator.next();
            
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(directory)) {
                for (Path path : stream) {
                    if (Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS) &&
                        path.getFileName().toString().endsWith("debian.yaml")) {
                        file_paths.add(path);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(DebianGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
            }
            if (file_paths.size() > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> next() {
        if (hasNext()) {
            final Path file_path = file_paths.remove(file_paths.size() - 1);
            Yaml yaml = new Yaml();
            try (Reader reader = Files.newBufferedReader(file_path)) {
                final Map<String, Object> file = yaml.load(reader);
                fixRegistry(file_path, file);
                return file;
            } catch (Exception ex) {
                Logger.getLogger(DebianGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
            }
        }        
        return null;
    }

    @Override
    public void close() throws IOException {
        directories.close();
    }
    
    /* Some files miss bio.tools identifier.
     * Method injects enclosed direcory as te id.
     */
    private void fixRegistry(final Path file_path, final Map<String, Object> file) {
        final Object _registries = file.get("registries");
        List registries = null;
        String id = null;
        if (_registries instanceof List) {
            registries = (List)_registries;
            for (Object _registry : registries) {
                if (_registry instanceof Map) {
                    final Map egistry = (Map)_registry;
                    final Object _id = egistry.get("entry");
                    final Object registry = egistry.get("name");
                    if (_id instanceof String && "bio.tools".equals(registry)) {
                        id = _id.toString().toLowerCase().trim();
                        break;
                    }
                }
            }
        }
        if (id == null) {
            final Path dir = file_path.getParent();
            id = dir.getFileName().toString().toLowerCase().trim();
            if (registries == null) {
                registries = new ArrayList();
                file.put("registries", registries);
            }
            Map<String, Object> registry = new LinkedHashMap();
            registry.put("entry", id);
            registry.put("name", "bio.tools");
            registries.add(registry);
        }

    }
}
